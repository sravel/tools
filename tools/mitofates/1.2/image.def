Bootstrap: docker
From: spack/ubuntu-jammy:latest
Stage: build

%post
  TOOL=mitofates
  VERSION=1.2

  # Create the manifest file for the installation in /opt/spack-environment
  mkdir /opt/spack-environment && cd /opt/spack-environment
  cat << EOF > spack.yaml
spack:
  specs:
  - ${TOOL}@${VERSION}
  concretizer:
    unify: true
  config:
    install_tree: /opt/software
  view: /opt/views/view
EOF

  # Install all the required software
  . /opt/spack/share/spack/setup-env.sh
  spack -e . concretize
  spack -e . install
  spack gc -y
  spack env activate --sh -d . >> /opt/spack-environment/environment_modifications.sh

  # Strip the binaries to reduce the size of the image
  find -L /opt/views/view/* -type f -exec readlink -f '{}' \; | \
    xargs file -i | \
    grep 'charset=binary' | \
    grep 'x-executable\|x-archive\|x-sharedlib' | \
    awk -F: '{print $1}' | xargs strip

Bootstrap: docker
From: ubuntu:22.04
Stage: final

%files from build
  /opt/spack-environment /opt
  /opt/software /opt
  /opt/views /opt
  /opt/spack-environment/environment_modifications.sh /opt/spack-environment/environment_modifications.sh

%post
  # For Perl
  export DEBIAN_FRONTEND=noninteractive
  apt-get -qq update && apt-get -qq upgrade -y
  apt-get -qq install -y locales
  localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

  # Symlink the old view location
  ln -s /opt/views/view /opt/view

  # Modify the environment without relying on sourcing shell specific files at startup
  cat /opt/spack-environment/environment_modifications.sh >> $SINGULARITY_ENVIRONMENT

%environment
  export LANG=en_US.utf8

%test
  MitoFates.pl 2>&1 | grep Usage
