Bootstrap: docker
From: ubuntu:20.04

%labels
    Author IFB
    Version 2.7.9

%post
    export PYCHOPPER_VERSION=2.7.9

    export PATH=/opt/conda/bin:$PATH

    # Install dependencies
    ## From apt
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*

    ## From conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    rm Miniconda3-latest-Linux-x86_64.sh

    conda update -q -y -n base conda
    conda install -q -y -n base conda-libmamba-solver
    conda config --set solver libmamba
    conda install -q -y -c nanoporetech -c conda-forge -c bioconda python=3.8 pychopper=${PYCHOPPER_VERSION}
    conda clean -a -y -q

%environment
    export PATH=/opt/conda/bin:$PATH

%runscript
    echo "Pychopper requires a license agreement: https://github.com/epi2me-labs/pychopper/blob/master/LICENSE.md"
    exec "$@"

%test
    export PATH=/opt/conda/bin:$PATH
    pychopper -h

