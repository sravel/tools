BootStrap: docker
From: centos:7

%files
    /src/coot-phenix/0.9.8.1-1.18.2/phenix-installer-1.18.2-3874-intel-linux-2.6-x86_64-centos6.tar.gz /tmp

%environment
    export LC_ALL=C
    _VERSION_SOFT=1.18.2-3874
    _VERSION_Coot=0.8.9.1
    _APP_DIR=/opt
    . ${_APP_DIR}/phenix-${_VERSION_SOFT}/phenix_env.sh
    export PATH=${_APP_DIR}/coot-${_VERSION_Coot}/bin:$PATH

%post
    # variable
    _VERSION_SOFT=1.18.2-3874
    _VERSION_SOFT_suffix=intel-linux-2.6-x86_64-centos6
    _VERSION_Coot=0.8.9.1
    _VERSION_Coot_suffix="binary-Linux-x86_64-scientific-linux-7.4-python-gtk2"
    _APP_DIR=/opt

    # locale setting
    #yum -y -q reinstall glibc-common

    # utilities
    yum -y -q install epel-release wget bsdtar

    # phenix system dependencies
    yum -y -q install fontconfig libXdamage libXext libgomp libxcb python-argparse libjpeg62 compat-guile18 tcl

    # install phenix
    cd /tmp
    _FILE_NAME=phenix-installer-${_VERSION_SOFT}-${_VERSION_SOFT_suffix}.tar.gz
    bsdtar xzf "${_FILE_NAME}"
    cd phenix-installer-${_VERSION_SOFT}-intel-linux-2.6-x86_64-centos6
    ./install --prefix="${_APP_DIR}"
    cd /tmp
    rm -rf /tmp/phenix-installer-${_VERSION_SOFT}-intel-linux-2.6-x86_64-centos6
    rm "/tmp/${_FILE_NAME}"

    # coot system dependencies
    yum -y -q install libpng libXmu libGL libGLU libgnomecanvas fftw2

    # install coot
    _DIRECTORY_Coot=${_APP_DIR}/coot-${_VERSION_Coot}
    _FILE_NAME_Coot=coot-${_VERSION_Coot}-${_VERSION_Coot_suffix}.tar.gz
    mkdir -p "${_DIRECTORY_Coot}"
    wget -q -O ${_FILE_NAME_Coot} "https://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot/binaries/release/${_FILE_NAME_Coot}"
    tar axf "${_FILE_NAME_Coot}" -C "${_DIRECTORY_Coot}" --strip 1
    chown -R root. "${_DIRECTORY_Coot}"
    chmod -R 777 "${_DIRECTORY_Coot}"

%runscript
    exec "$@"

%labels
    Author IGBMC

%test
    _APP_DIR=/opt

    _VERSION_SOFT=1.18.2-3874
    . "${_APP_DIR}/phenix-${_VERSION_SOFT}/phenix_env.sh"
    phenix.about 2>&1 | grep "Version: ${_VERSION_SOFT%-*}"

    _VERSION_Coot=0.8.9.1
    export PATH=${_APP_DIR}/coot-${_VERSION_Coot}/bin:$PATH
    coot --version 2>&1 | grep "${_VERSION_Coot} (revision-count"
