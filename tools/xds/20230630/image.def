BootStrap: docker
From: debian:10

%labels
    Author IGBMC

%files
    /src/xds/20230630/ccp4-8.0.011-shelx-arpwarp-linux64.tar.gz /opt

%post
    # install system dependencies
    apt-get -y -qqqq update
    apt-get -y -qqqq install bsdtar\
    csh\
    curl\
    ffmpeg\
    grace\
    imagemagick\
    libcgi-pm-perl\
    libfontconfig1-dev\
    libgl1-mesa-dev\
    libglu1-mesa-dev\
    libncurses5\
    libsm6\
    libxcursor1\
    libxcomposite1\
    libxdamage1\
    libxext-dev\
    libxmu6\
    libxrandr2\
    libxrender-dev\
    libxt6\
    libxxf86vm1\
    mesa-utils\
    openbabel\
    openjdk-11-jre\
    php-cli\
    povray\
    pymol\
    tcsh\
    texlive-latex-extra\
    wget \
    zip

    # build and install ccp4
    cd /opt
    bsdtar xzf ccp4-8.0.011-shelx-arpwarp-linux64.tar.gz
    touch $HOME/.agree2ccp4v6
    cd /opt/ccp4-8.0
    ./BINARY.setup
    rm /opt/ccp4-8.0.011-shelx-arpwarp-linux64.tar.gz

    # load ccp4 environment
    . /opt/ccp4-8.0/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    # install xds dependencies
    apt-get -y -qqqq install\
    build-essential\
    python-pip\
    zlib1g-dev\
    cmake #for neggia

    # install xds
    cd /opt
    wget -q https://xds.mr.mpg.de/XDS-INTEL64_Linux_x86_64.tar.gz
    tar xzf XDS-INTEL64_Linux_x86_64.tar.gz
    export PATH=/opt/XDS-INTEL64_Linux_x86_64:$PATH
    rm /opt/XDS-INTEL64_Linux_x86_64.tar.gz

    # install XDS-Viewer, XDSSTAT, XDSCC12, XDSGUI, XSCALE_ISOCLUSTER
    # dependencies first
    apt-get -y -qqqq install\
    libqt5test5 libqt5opengl5 libqt5printsupport5\
    xxdiff

    mkdir -p /opt/xds_gui && cd /opt/xds_gui
    wget -N -r -np -nd -e robots=off -R "index.html*" -R "robots*" https://wiki.uni-konstanz.de/pub/linux_bin/xdsgui || true
    chmod a+x *
    ln -s xdscc12 xscalecc12
    ln -s XDS-viewer xdsviewer
    ln -s XDS-viewer xds-viewer
    export PATH=/opt/xds_gui:$PATH

    cd /tmp
    wget -q https://github.com/dectris/neggia/tarball/master
    tar xzf master
    cd dectris-neggia-*
    mkdir build
    cd build
    cmake ..
    make
    mkdir /opt/neggia
    mv src/dectris/neggia/libneggia_static.a /opt/neggia
    mv src/dectris/neggia/plugin/dectris-neggia.so /opt/neggia
    cd ../..
    rm -rf dectris-neggia-*

    # install XH5ToXds
    cd /opt/
    wget -q https://media.dectris.com/H5ToXds_1.1.0_x86_64_1.zip
    unzip H5ToXds_1.1.0_x86_64_1.zip
    gunzip H5ToXds_1.1.0_x86_64\ \(1\).gz
    mv H5ToXds_1.1.0_x86_64\ \(1\) /usr/local/bin/H5ToXds
    chmod +x /usr/local/bin/H5ToXds
    rm H5ToXds_1.1.0_x86_64_1.zip 

    #fix for weird alias-related error messages 'alias: could not parse "pushd $CCP4>/dev/null": 1:12: > is not a valid word'
    sed -i "s/^alias.*$//" /opt/ccp4-8.0/bin/ccp4.setup-sh

    # cleanup
    apt-get remove -y bsdtar wget zip
    rm -rf /var/lib/apt/lists/*



%environment
    export LC_ALL=C
    export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/

    . /opt/ccp4-8.0/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    export PATH=/opt/XDS-INTEL64_Linux_x86_64:$PATH
    export PATH=/opt/xds_gui:$PATH

%test
    #very basic test for xdsgui
    test -f /opt/xds_gui/xdsgui

