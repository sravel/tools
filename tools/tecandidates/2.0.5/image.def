BootStrap: docker
From: mambaorg/micromamba:focal

%post
    export TOOLS_VERSION=2.0.5

    ## From apt
    export DEBIAN_FRONTEND=noninteractive
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget tar zip locales libtbb2

    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

    ## Deps
    export MAMBA_ROOT_PREFIX=/opt/conda
    micromamba install -q -y -c conda-forge -c bioconda -c defaults -n base \
        bioconda::bedtools=2.25.0 \
        bioconda::perl-bioperl \
        bioconda::samtools=1.9 \
        bioconda::trinity=2.4.0

    ## Bowtie2
    cd /opt/
    wget -q http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.3.2/bowtie2-2.3.2-linux-x86_64.zip
    unzip bowtie2-2.3.2-linux-x86_64.zip
    mkdir -p /usr/local/bin/bowtie2
    mv bowtie2-2.3.2/bowtie2* /usr/local/bin/bowtie2
    rm -rf bowtie2-2.3.2-linux-x86_64.zip bowtie2-2.3.2/

    ## TEcandidates
    cd /opt/
    wget -q https://github.com/mobilomics/TEcandidates/archive/refs/tags/v${TOOLS_VERSION}.tar.gz
    tar -xzf v${TOOLS_VERSION}.tar.gz
    mv TEcandidates-${TOOLS_VERSION}/TEcandidates_v2.0.3.sh /usr/local/bin/TEcandidates.sh
    chmod +x /usr/local/bin/TEcandidates.sh
    rm -rf v${TOOLS_VERSION}.tar.gz /opt/TEcandidates-*

    # Cleaning
    apt-get -qq autoremove -y
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*
    micromamba clean -y --all

%environment
    export PATH=/usr/local/bin/bowtie2:/opt/conda/bin:$PATH
    export MAMBA_ROOT_PREFIX=/opt/conda
    export LANG=en_US.utf8


%test
    bedtools --version
    perl -MBio::Root::Version -e 'print $Bio::Root::Version::VERSION,"\n"'
    bowtie2 --version
    samtools --version
    Trinity --version
    TEcandidates.sh
