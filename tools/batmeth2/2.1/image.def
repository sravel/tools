BootStrap: docker
From: ubuntu:22.04

%labels
    Author IFB

%post
    BATMETH_VERSION="107c9600666b4df43d7a978f11a23444b8138c96"

    ## From apt
    export DEBIAN_FRONTEND=noninteractive
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget zip \
        build-essential \
        libgsl-dev \
        zlib1g-dev \
        locales
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*

    ### For perl scripts
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

    ## Conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    rm Miniconda3-latest-Linux-x86_64.sh

    export PATH=/opt/conda/bin:$PATH
    conda update -q -y conda
    conda install -c conda-forge -y mamba

    ## Deps
    mamba install -q -y -c conda-forge -c bioconda -c defaults \
	    bioconda::fastp=0.23.2 \
        bioconda::samtools=1.17


    # BatMeth2
    cd /opt
    wget https://github.com/GuoliangLi-HZAU/BatMeth2/archive/${BATMETH_VERSION}.zip
    unzip ${BATMETH_VERSION}.zip
    cd  BatMeth2-${BATMETH_VERSION}

    ./configure
    make
    make install
    mv bin/* /usr/local/bin/

    # Cleaning
    rm -rf /opt/${BATMETH_VERSION}.zip
    rm -rf /opt/BatMeth2-${BATMETH_VERSION}
    conda remove -y mamba
    conda clean -y --all

%environment
    export PATH=/opt/conda/bin:/opt/ProtHint/bin:$PATH
    export LANG=en_US.utf8

%test
    BatMeth2 --version
    samtools --version
    fastp --version
