#!/usr/bin/env bash

function add_bot_message_to_mr() {
  if [ $CI_MERGE_REQUEST_ID ]
  then
    curl --request POST --header "Authorization: Bearer $BOT_API_TOKEN" --data-urlencode "body=$1"   https://gitlab.com/api/v4/projects/${CI_MERGE_REQUEST_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/notes
  fi
}

function validate_yaml() {
  schema=$1
  yaml=$2

  # yamale: validate yaml file based on schema
  # grep: get "Error validating data" line and all lines after
  # grep: remove "Error validating data" line to get only explicit errors line
  # sed: remove heading and traling spaces
  # awk: remove any blank lines
  yamale -s $schema -n 1 $yaml 2> /dev/null | grep -A 1000 "Error validating data" | grep -v "Error validating data" | sed '/^$/d' | awk '{$1=$1};1'
}

function run() {
  CMD=$1
  VERBOSE=$2
  if [ "$REMOTE_MODE" = true ]; then
    if [ "$VERBOSE" != false ]; then
      echo "RUN: ssh ${CLUSTER_USER}@${CLUSTER_HOST} $CMD"
    fi
    ssh ${CLUSTER_USER}@${CLUSTER_HOST} $CMD
  else
    if [ "$VERBOSE" != false ]; then
      echo "RUN: $CMD"
    fi
    eval $CMD
  fi
}

run "pwd"
if [ $? -ne 0 ]
then
    echo -e "\xE2\x9D\x8C Can't run command or connect to remote host"
    exit 42
fi
