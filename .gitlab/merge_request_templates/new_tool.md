## Contributor checklist

* [ ] This tool is not an isolated library that could be installed manually by a user or that should be included in a langage package (Python, R, Perl, etc.)
* [ ] The conda env or apptainer definition could be rerun at any time and would result in the exact same tool version

Please, review our [Best pratices](https://gitlab.com/ifb-elixirfr/cluster/tools#best-practices) for more informations